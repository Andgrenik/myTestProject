/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. 
  		Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. 
  		Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и 
  		показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка 
  		формата:

      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, 
  		а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/

	let wrapper = document.getElementById('wrap');   // блок оформления шапки таблицы
	let table = document.createElement("table");
	wrapper.appendChild(table);

	let tr1 = document.createElement('tr');
	table.appendChild(tr1);

	let th1 = document.createElement('th');
	tr1.appendChild(th1);
	th1.innerHTML = 'Company';

	let th2 = document.createElement('th');
	tr1.appendChild(th2);
	th2.innerHTML = 'Balance';
	th2.addEventListener('click', sortBalance);
	th2.classList.add('pointer');

	let th3 = document.createElement('th');
  	tr1.appendChild(th3);
 	th3.innerHTML = 'Registered';

 	let th4 = document.createElement('th');
  	tr1.appendChild(th4);
 	th4.innerHTML = 'Adress';

	let th5 = document.createElement('th');
	tr1.appendChild(th5);
	th5.innerHTML = 'Employers';

	let th6 = document.createElement('th');
  	tr1.appendChild(th6);
  	th6.innerHTML = 'Show employers';

  	let tbody = document.createElement('tbody');
  	table.appendChild(tbody);

let promise = fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2')
  .then (response => response.json())
  .then (ourFunc);

  function ourFunc (promise) {
    promise.forEach(function(item) {

	  	tr2 = document.createElement('tr');    //создание ячеек таблицы и их наполнение
	    tbody.appendChild(tr2);

	    let td1 = document.createElement('td');
	    tr2.appendChild(td1);
	    td1.innerHTML = item.company;

	    let td2 = document.createElement('td');
	    tr2.appendChild(td2);
	    td2.innerHTML = item.balance;

	    let td3 = document.createElement('td');
	    tr2.appendChild(td3);
	    td3.innerHTML = item.registered;

	    let td4 = document.createElement('td');
	    tr2.appendChild(td4);
	    td4.innerHTML = 'показать адрес';
	    td4.classList.add('pointer');
	    td4.addEventListener('click', function showAdress(){
			alert (`${item.company} \n ${item.address.house} \n ${item.address.street} \n ${item.address.state} \n ${item.address.city} \n ${item.address.country}`);
	    });

	    let ourEmployers = 0;
	    for (j in item.employers){
	    	ourEmployers++;
	    }

	    let td5 = document.createElement('td');
	    tr2.appendChild(td5);
	    td5.innerHTML = ourEmployers;

	    let td6 = document.createElement('td');
	    tr2.appendChild(td6);
	    td6.innerHTML = 'показать сотрудников';
	    td6.classList.add('pointer');
	    td6.addEventListener('click', function createTableTwo(){
	    	
	    	let link = document.createElement('a');
	    	link.href = '#';
	    	let textnode = document.createTextNode("<-- Назад к списку компаний");
	    	link.appendChild(textnode);
	    	document.body.insertBefore(link, document.body.firstChild);
	    	link.addEventListener('click', function hideTable(){
	    		document.location.reload(true);
	    	})

	    	let span = document.createElement('span');
	    	span.innerHTML = `Company name: ${item.company}`;
	    	document.body.insertBefore(span, wrapper);

	    	let tableEmployers = document.createElement('table');
	    	wrapper.replaceChild(tableEmployers, table);

	    	let tr1_1 = document.createElement('tr');
			tableEmployers.appendChild(tr1_1);

	    	let th1_1 = document.createElement('th');
			tr1_1.appendChild(th1_1);
			th1_1.innerHTML = 'Name';

			let th1_2 = document.createElement('th');
			tr1_1.appendChild(th1_2);
			th1_2.innerHTML = 'Gender';
			th1_2.addEventListener('click', sortGender);
			th1_2.classList.add('pointer');

			let th1_3 = document.createElement('th');
			tr1_1.appendChild(th1_3);
			th1_3.innerHTML = 'Age';
			th1_3.addEventListener('click', sortAge);
			th1_3.classList.add('pointer');

			let th1_4 = document.createElement('th');
			tr1_1.appendChild(th1_4);
			th1_4.innerHTML = 'Contacts';

			let tbody_1 = document.createElement('tbody');
  			tableEmployers.appendChild(tbody_1);

			for (i in item.employers){
				tr2_1 = document.createElement('tr');    //создание ячеек второй таблицы и их наполнение
	    		tbody_1.appendChild(tr2_1);

			    let td1_1 = document.createElement('td');
			    tr2_1.appendChild(td1_1);
			    td1_1.innerHTML = item.employers[i].name;

			    let td2_1 = document.createElement('td');
			    tr2_1.appendChild(td2_1);
			    td2_1.innerHTML = item.employers[i].gender;

			    let td3_1 = document.createElement('td');
			    tr2_1.appendChild(td3_1);
			    td3_1.innerHTML = item.employers[i].age;


			    let td4_1 = document.createElement('td');
			    tr2_1.appendChild(td4_1);
			    td4_1.innerHTML = item.employers[i].emails + '<br>' + item.employers[i].phones;

			}

			let inputSearch = document.createElement("input");
			inputSearch.setAttribute("type", "text");
			wrapper.appendChild(inputSearch);

			let submitButton = document.createElement('input');
			submitButton.setAttribute('type', 'submit');
			wrapper.appendChild(submitButton);
			submitButton.setAttribute('value', 'search');
			submitButton.addEventListener('click', searchForName);

			function searchForName(){
				for (let i = 0; i < tbody_1.children.length; i++){
					tbody_1.children[i].childNodes[0].style.borderColor = "grey";
					if (inputSearch.value == tbody_1.children[i].childNodes[0].innerHTML){
						tbody_1.children[i].childNodes[0].style.borderColor = "red";
					}
				}
			}

			function sortAge(){    // сортировка по возрасту
				tableEmployers.removeChild(tbody_1);
				let rows = [];
				for (let i = 0; i < tbody_1.children.length; i++){
					let elem = tbody_1.children[i];
					rows.push({
						value: elem.childNodes[2].innerHTML,
						elem: elem
					});
				}
				rows.sort(function(a, b) {
					return a.value - b.value;
				});
				for (let i = 0; i < rows.length; i++) {
					tbody_1.appendChild(rows[i].elem);
				}
				tableEmployers.appendChild(tbody_1);
			}

			function sortGender(){
				tableEmployers.removeChild(tbody_1);
				let rows = [];
				for (let i = 0; i < tbody_1.children.length; i++){
					let elem = tbody_1.children[i];
					rows.push({
						value: elem.childNodes[1].innerHTML.charAt(0),
						elem: elem
					});
				}
				rows.sort(function(a, b) {
					return a.value.charCodeAt() - b.value.charCodeAt();
				});
				console.log(rows);
				for (let i = 0; i < rows.length; i++) {
					tbody_1.appendChild(rows[i].elem);
				}
				tableEmployers.appendChild(tbody_1);
			}
	    });
    });
}

function sortBalance() {        //сортировка таблицы по Balance.
	table.removeChild(tbody);
	let rows = [];
	for (let i = 0; i < tbody.children.length; i++) {
		let elem = tbody.children[i];
		rows.push({
			value: elem.childNodes[1].innerHTML.slice(1),
			elem: elem
		});
	}
	rows.sort(function(a, b) {
		return a.value - b.value;
	});
	for (let i = 0; i < rows.length; i++) {
		tbody.appendChild(rows[i].elem);
	}
	table.appendChild(tbody);
};
