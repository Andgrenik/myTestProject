/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }


     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.


*/


var x = fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2")
.then(response => response.json())
.then (random)
.then (
  response1 => {
    return fetch(`http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2`)
      .then(response => response.json())
      .then(
        response2 => {
            let  {name, age, gender} = response1;
            return {
              name,
              age,
              gender,
              friends: response2
            };
        }
      )
  })
.then( res => console.log(res) )

function random(obj) {
  console.log(obj);
  let man = obj[Math.floor(Math.random()*obj.length)];
  return man;
};

